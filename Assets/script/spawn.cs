﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class spawn : MonoBehaviour, IVirtualButtonEventHandler{
	
	public GameObject[] zombies;
	public int amount;
	public Transform clone;

	private Vector3 spawnPoint;
	private GameObject vbButtonObject;

	
	void start(){
		
		vbButtonObject.GetComponent<VirtualButtonBehaviour> ().RegisterEventHandler (this);
		
	}
	
	void Update () {
		
		vbButtonObject = GameObject.Find ("spawnButton");
		zombies = GameObject.FindGameObjectsWithTag ("zombie");

		amount = zombies.Length;
		
		if (amount != 20) {
		
			InvokeRepeating ("spawnZombie",1,5f);
		
		}
		
	}
	
	public void OnButtonPressed(VirtualButtonAbstractBehaviour vb){
		
		Debug.Log ("herehere");
		if (amount != 20) {
			Debug.Log ("hahahahahahah");
			InvokeRepeating ("spawnZombie",1,5f);
			
		}
		
		
	}
	
	public void OnButtonReleased(VirtualButtonAbstractBehaviour vb){
		
		
	}
	
	
	
	
	
	void spawnZombie(){
		
		spawnPoint.x = clone.position.x;
		spawnPoint.y = clone.position.y;
		spawnPoint.z = clone.position.z;

		int num = Random.Range (0,zombies.Length-1);

		//Instantiate (zombies [UnityEngine.Random.Range (0, zombies.Length - 1)], spawnPoint, Quaternion.identity);
		Instantiate (zombies [num], spawnPoint, Quaternion.identity);
		Vector3 scale = zombies [num].transform.localScale;
		scale.y = 0.2f;
		scale.x = 0.2f;
		scale.z = 0.2f;
		zombies [num].transform.localScale = scale;
		CancelInvoke ();
		
	}
}

